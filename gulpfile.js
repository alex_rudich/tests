'use strict';

const { src, dest, watch, series, parallel } = require('gulp'),
        babel = require('gulp-babel'),
        concat = require('gulp-concat'),
        ngAnnotate = require('gulp-ng-annotate-patched'),
        sourcemaps = require('gulp-sourcemaps'),
        htmlmin = require('gulp-htmlmin'),
        gulpIf = require('gulp-if'),
        del = require('del'),
        sass = require('gulp-sass'),
        cleanCSS = require('gulp-clean-css'),
        size = require('gulp-size'),
        strip = require('gulp-strip-debug'),
        browserSync = require('browser-sync').create();

const env = { release: false };
const appPath = 'build';

const paths = {
    scripts: {
        src: [
            'node_modules/angular/angular.min.js',
            'node_modules/angular-animate/angular-animate.min.js',
            'node_modules/angular-touch/angular-touch.min.js',
            'node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls.js',
            'src/app.js',
            'src/**/*.js'
        ],
        dest: appPath + '/scripts'
    },
    styles: {
        src: 'src/sass/**/*.scss',
        dest: appPath + '/css'
    },
    static: {
        src: 'src/images/**/*',
        dest: appPath + '/images'
    },
    html: {
        src: 'src/index.html',
        dest: appPath
    },
    app: {
        file: 'app.js',
        dest: appPath,
        clean: 'build/**/*'
    }
};

function buildJS() {
    const pipeBabel = babel({
        plugins: [
            ['@babel/plugin-transform-object-assign'],
            ['@babel/plugin-proposal-object-rest-spread'],
            ['@babel/plugin-transform-classes']
        ],
        presets: [
            ['@babel/env']
        ],
        compact: false,
        minified: env.release,
    });
    return src(paths.scripts.src, {allowEmpty: true})
        .pipe(size({title: 'JS'}))
        .pipe(sourcemaps.init())
        .pipe(gulpIf(isAppFile(), pipeBabel))
        .pipe(gulpIf(env.release && isAppFile(), ngAnnotate()))
        .pipe(concat(paths.app.file))
        .pipe(gulpIf(env.release && isAppFile(), strip()))
        .pipe(sourcemaps.write(''))
        .pipe(dest(paths.scripts.dest));

    function isAppFile() {
        return (file) => file.path.match(/src/);
    }
}

function buildCSS() {
    return src(paths.styles.src)
        .pipe(size({title: 'CSS'}))
        .pipe(sourcemaps.init())
        .pipe(sass({noCache: true, outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulpIf(env.release, cleanCSS({level: 2})))
        .pipe(sourcemaps.write(''))
        .pipe(dest(paths.styles.dest))
        .pipe(browserSync.stream());
}

function buildEntry() {
    return src(paths.html.src)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(dest(paths.html.dest));
}

function copyStatic() {
    return src(paths.static.src)
        .pipe(size({title: 'Static files'}))
        .pipe(dest(paths.static.dest));
}

function clean() {
    return del(paths.app.clean, {
        force: true
    });
}

function serve() {
    browserSync.init({
        server: paths.app.dest,
        reloadDelay: 200,
        reloadOnRestart: true,
        notify: false,
        ghostMode: {
            clicks: false,
            forms: false,
            scroll: false
        }
    });
    browserSync.watch(paths.app.dest, browserSync.reload);
}

function watchCSS() {
    watch(paths.styles.src, buildCSS);
}

function watchJS() {
    watch(paths.scripts.src, buildJS);
}

function watchEntry() {
    watch(paths.html.src, buildEntry);
}

function envRelease(done) {
    env.release = true;
    done();
}

let build = series(clean, series(buildCSS, buildJS, buildEntry, copyStatic));
let _watch = series(build, parallel(watchCSS, watchJS, watchEntry, serve));

exports.clean = clean;
exports.release = series(envRelease, build);
exports.build = build;
exports.watch = _watch;
exports.default = _watch;