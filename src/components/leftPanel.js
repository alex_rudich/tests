class LeftPanel {
    constructor(dataService) {
        this.dataService = dataService;
    }

    $onInit() {
        console.log('LeftPanel');
    }

    addCar() {
        this.dataService.actions = true;
        this.dataService.car = {vehicleID: '', name: '',  model: '', year: '', type: ''};
    }

    editCar(ID) {
        this.dataService.actions = true;
        this.dataService.car = {...this.dataService.cars.find(el => el.ID === ID)};
    }

    deleteCar(ID) {
        this.dataService.deleteCar(ID);
        this.dataService.actions = false;
        this.dataService.car = undefined;

    }
}

const tpl = `
    <div class="d-flex mt-3">
        <h2 class="mr-3">Cars</h2>
        <button class="btn btn-primary" ng-click="vm.addCar()">Add Car</button>
    </div>
    <div class="m-2" ng-if="!vm.dataService.cars.length">
        No cars here!
    </div>
    <div class="m-2" ng-if="vm.dataService.cars.length">
        <div ng-repeat="car in vm.dataService.cars track by $index">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <div class="border border-secondary rounded p-1">Vehicle ID: {{ car.vehicleID }}</div>
                </div>
                <div class="col-sm-8">
                    <div class="border border-secondary rounded p-1">Name: {{ car.name }}</div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-4">
                    <div class="border border-secondary rounded p-1">Model: {{ car.model }}</div>
                </div>
                <div class="col-sm-4">
                    <div class="border border-secondary rounded p-1">Year: {{ car.year }}</div>
                </div>
                <div class="col-sm-4">
                    <div class="border border-secondary rounded p-1">Type: {{ car.type }}</div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col">
                    ID: {{ car.ID }}
                    <button class="btn btn-success" ng-click="vm.editCar(car.ID)">Edit Car</button>
                    <button class="btn btn-danger" ng-click="vm.deleteCar(car.ID)">Delete Car</button>
                </div>
            </div>
            <hr />
        </div>
    </div>
`;
mainApp.component('leftPanel', {
    template: tpl,
    controller: LeftPanel,
    controllerAs: 'vm',
    bindings: {}
});