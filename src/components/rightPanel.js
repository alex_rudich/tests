class RightPanel {
    constructor(dataService) {
        this.dataService = dataService;
        this.years = Array.from(new Array(31), (el, i) => i + 1990);
        this.types = ['VW','Audi','Ford','Opel','BMW','Daewoo'];
    }

    $onInit() {
        console.log('RightPanel');
    }

    saveCar() {
        this.dataService.saveCar();
        this.closeCar();
    }

    closeCar() {
        this.dataService.actions = false;
        this.dataService.car = undefined;
    }
}

const tpl = `
    <div class="d-flex mt-3">
        <h2 class="mr-3">Car</h2>
        <button class="btn btn-danger" ng-click="vm.closeCar()" ng-if="vm.dataService.actions">Close Form</button>
    </div>
    <div class="m-2" ng-if="!vm.dataService.actions">
        Car has not selected yet!
    </div>
    <div class="m-2" ng-if="vm.dataService.actions">
        <form novalidate name="carForm">
            <uib-tabset active="active">
                <uib-tab index="0" heading="Main data">
                    <div class="row my-3">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="vehicle-id">Vehicle ID</label>
                                <input type="text" class="form-control" id="vehicle-id" ng-model="vm.dataService.car.vehicleID" ng-required="true">
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="form-group">
                                <label for="vehicle-name">Vehicle name</label>
                                <input type="text" class="form-control" id="vehicle-name" ng-model="vm.dataService.car.name" ng-required="true">
                            </div>
                        </div>
                    </div>
                </uib-tab>
                <uib-tab index="1" heading="Additional data">
                    <div class="row my-3">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="vehicle-model">Vehicle model</label>
                                <input type="text" class="form-control" id="vehicle-model" ng-model="vm.dataService.car.model" ng-required="true">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="vehicle-year">Vehicle year</label>
                                <select class="form-control" id="vehicle-year" ng-model="vm.dataService.car.year" ng-required="true">
                                    <option value="" selected disabled>Select year</option>
                                    <option ng-repeat="year in vm.years" ng-value="year">{{ year }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="vehicle-type">Vehicle type</label>
                                <select class="form-control" id="vehicle-type" ng-model="vm.dataService.car.type" ng-required="true">
                                    <option value="" selected disabled>Select type</option>
                                    <option ng-repeat="type in vm.types" ng-value="type">{{ type }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </uib-tab>
            </uib-tabset>
            <button class="btn btn-success" ng-click="vm.saveCar()" ng-disabled="carForm.$invalid">Save Car</button>
        </form>
    </div>
`;
mainApp.component('rightPanel', {
    template: tpl,
    controller: RightPanel,
    controllerAs: 'vm',
    bindings: {}
});