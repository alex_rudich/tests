class Cars {
    constructor() {
        this.cars = [];
        this.car = undefined;
        this.actions = false;
    }

    saveCar() {
        if (this.car.ID === undefined) {
            const ID = this.cars.length ? Math.max(...(this.cars.map(el => el.ID))) + 1 : 0;
            this.cars.push({ID, ...this.car});
        } else {
            this.cars.forEach(el => {
                if (el.ID === this.car.ID) {
                    for (const key in this.car) {
                        if (this.car.hasOwnProperty(key) && key !== 'ID') el[key] = this.car[key];
                    }
                }
            });
        }
    }

    deleteCar(ID) {
        this.cars = this.cars.filter(el => el.ID !== ID);
    }
}
mainApp.service('dataService', Cars);
